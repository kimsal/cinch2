#!/bin/bash
#user, role, requestmap, securityinfo, aclsid, aclobjectidentity,
#acletry, aclclass, persistentlogin, register, registrationcode,
cd ../
yes | grails s2ui-override layout
yes | grails s2ui-override auth
yes | grails s2ui-override user gov.ncdcr.cinch 
yes | grails s2ui-override role gov.ncdcr.cinch 
yes | grails s2ui-override requestmap gov.ncdcr.cinch 
yes | grails s2ui-override securityinfo gov.ncdcr.cinch 
yes | grails s2ui-override aclsid gov.ncdcr.cinch 
yes | grails s2ui-override aclobjectidentity gov.ncdcr.cinch 
yes | grails s2ui-override aclentry gov.ncdcr.cinch 
yes | grails s2ui-override aclclass gov.ncdcr.cinch 
yes | grails s2ui-override persistentlogin gov.ncdcr.cinch 
yes | grails s2ui-override register gov.ncdcr.cinch 
yes | grails s2ui-override registrationcode gov.ncdcr.cinch 
