package gov.ncdcr.cinch

import org.apache.tika.parser.AutoDetectParser
import org.apache.tika.metadata.Metadata
import org.apache.tika.sax.BodyContentHandler
import grails.converters.JSON

class IndexService {

	static rabbitQueue = 'index'
	
	def handleMessage(fileId) {
		def fileToIndex = File.get(fileId)
		def f = new java.io.File(fileToIndex.path + "/" + fileToIndex.downloadedName)
		def inputStream = new FileInputStream(f)
		def tika = new org.apache.tika.Tika()
		def fileMetadata = fileToIndex?.metadata
		if(fileMetadata==null)
		{
			fileMetadata = new gov.ncdcr.cinch.Metadata()
			fileMetadata.file = fileToIndex
			fileMetadata.save()
			fileToIndex.metadata = fileMetadata
		}
		try {
			def parser = new AutoDetectParser()
			def metadata = new Metadata()
			def handler = new BodyContentHandler(20*1024*1024)
			parser.parse(inputStream, handler, metadata)
			def detectedMime = tika.detect(f)
			fileToIndex.mime = detectedMime
			def attributes = [:]
			metadata.names().each { name->
				attributes[name] = metadata.get(name)
				//render("name="+name + " and value = "+metadata.get(name) + "<BR>")
			}
			def attr = attributes as JSON
			fileToIndex.metadata.json = attr.toString()
			fileToIndex.metadata.rawText = handler.toString()

		} catch (Exception e)
		{
			render(e)
		}
	}
}
