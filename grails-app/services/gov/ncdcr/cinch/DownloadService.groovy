package gov.ncdcr.cinch
import org.apache.commons.io.FilenameUtils

class DownloadService {

	static rabbitQueue = 'download'

    void handleMessage(fileToDownloadId) {
		if (fileToDownloadId==null)
		{
			return
		}
		Thread th = Thread.currentThread()
		println "thread id = "+th.id  +  " : " + fileToDownloadId + " : " + new Date()
		def fileToDownload = File.get(fileToDownloadId)

		def downloadedName = th.id + "_"+fileToDownload.url.encodeAsMD5() + "_"+System.currentTimeMillis()
		def newname = fileToDownload.path +  downloadedName
		newname += "." + (FilenameUtils.getExtension(fileToDownload.url) ?: "pdf")
		def file = new java.io.File(newname)
//		println "downloading "+fileToDownload.url
		def u = fileToDownload.url
		def attempt = new Attempt(file: fileToDownload)
		try {
			use (FileBinaryCategory)
			{
				file << u.toURL()
			}
			fileToDownload.downloadedName = downloadedName
			attempt.status = "done"
			fileToDownload.status = "done"
			fileToDownload.size = file.size()
		} catch (FileNotFoundException e)
		{
			attempt.status = "filenotfound"
			println e
		} catch (MalformedURLException e)
		{
			println e
			attempt.status = "malformed"
		} catch (SocketException e)
		{
			println e
			attempt.status = "timeout"
		} catch (Exception e)
		{
			println e
			attempt.status = "unknown"
		}
		attempt.validate()
	//	println attempt.errors
		attempt.save()
		fileToDownload.size = file.size()
		fileToDownload.addToAttempts(attempt)
		fileToDownload.validate()
	//	println fileToDownload.errors
		fileToDownload.save()
		if(fileToDownload.status=="done")
		{
			rabbitSend 'index', fileToDownload.id
		}
		def p2 = Pluraliz
    }

	def indexFile(file)
	{

	}
}
