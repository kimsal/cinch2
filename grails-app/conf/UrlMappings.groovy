class UrlMappings {

	static mappings = {
		"/" {
			controller = "page"
			action = "home"
		}

		"/admin" {
			controller = "user"
		}
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"500"(view:'/error')
	}
}
