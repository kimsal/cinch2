import gov.ncdcr.cinch.User

class BootStrap {

    def init = { servletContext ->
		def p = User.findByUsername("admin")
		if(p==null)
		{
			p = new User(username: "admin", password: "adminpass")
			p.enabled = true
			p.email = "mgkimsal@gmail.com"
			p.validate()
			println p.errors
			p.save()
			println "p="+p.username
		}
    }
    def destroy = {
    }
}
