package gov.ncdcr.cinch

import org.codehaus.groovy.grails.plugins.springsecurity.acl.AclClass
import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class AclClassController extends grails.plugins.springsecurity.ui.AclClassController {

	protected String lookupClassName() { AclClass.name }

	protected Class<?> lookupClass() { AclClass }
}
