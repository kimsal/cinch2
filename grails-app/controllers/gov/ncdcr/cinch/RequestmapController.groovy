package gov.ncdcr.cinch

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class RequestmapController extends grails.plugins.springsecurity.ui.RequestmapController {
}
