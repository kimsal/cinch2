package gov.ncdcr.cinch

import grails.plugins.springsecurity.Secured

class HomeController {

	def springSecurityService

	def index() {
			redirect(controller:"page", action: "home")
	}

	@Secured(['IS_AUTHENTICATED_FULLY'])
	def home()
	{
		redirect(controller: "file")

	}
}
