package gov.ncdcr.cinch

import org.codehaus.groovy.grails.plugins.springsecurity.acl.AclEntry
import org.codehaus.groovy.grails.plugins.springsecurity.acl.AclSid
import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class AclEntryController extends grails.plugins.springsecurity.ui.AclEntryController {

	protected String lookupClassName() { AclEntry.name }

	protected Class<?> lookupClass() { AclEntry }

	protected Class<?> lookupAclSidClass() { AclSid }
}
