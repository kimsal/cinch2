package gov.ncdcr.cinch

import grails.plugins.springsecurity.Secured
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.apache.tika.Tika
import org.apache.tika.mime.MediaTypeRegistry
import org.apache.tika.mime.MediaType
import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.ParseContext
import org.apache.tika.parser.AutoDetectParser
import org.apache.tika.sax.BodyContentHandler
import grails.converters.JSON

@Secured(['IS_AUTHENTICATED_FULLY'])
class FileController {

	def springSecurityService

	def index() {
		def filesets = Fileset.findAllByUser(springSecurityService.currentUser).sort{it.dateCreated}
		[filesets:filesets]
	}
	
	def viewErrors()
	{
		def fileset = Fileset.findByUserAndId(springSecurityService.currentUser, params.id)
		[fileset:fileset]
	}
	def viewNotDone()
	{
		def fileset = Fileset.findByUserAndId(springSecurityService.currentUser, params.id)
		[fileset:fileset]
	}


	def upload()
	{
	 	def f = (CommonsMultipartFile)request.getFile("upload")
		def tempName = (System.currentTimeMillis()+f.originalFilename).encodeAsMD5()
		f.transferTo(new java.io.File("/tmp/foo_"+tempName))
		def uploadedFile = new java.io.File("/tmp/foo_"+tempName)
		def files = uploadedFile.text.split("\n")
		
		def fileset = new Fileset()
		fileset.name = f.originalFilename
		fileset.user = springSecurityService.currentUser
		fileset.save(flush: true)
		
		files.each{ fn->
			def newfile = new File()
			newfile.name = fn
			newfile.url = fn
			newfile.hash = fn.encodeAsMD5()
			newfile.path = "/tmp/"
			newfile.fileset = fileset
			newfile.validate()
			newfile.save()
			fileset.addToFiles(newfile)
		}
		fileset.filecount = files.size()
		fileset.save()
		fileset.files.each { runFile ->
			rabbitSend 'download', runFile?.id
		}

		redirect(action: "index")
	}


	def updatestatus =
	{
	   params.f.each { key, value->
		   if(value=="on" && (key as Long))
		   {
			   def file = File.get(key as Long)
			   if(file?.fileset.user == springSecurityService.currentUser)
			   {
				   rabbitSend 'download', file?.id
			   }
		   }
	   }
		redirect(action:"index"	)
	}

	def temp =
		{

			def f = new java.io.File("/Users/michael/john_hupper_resume.doc")
			def inputStream = new FileInputStream(f)
			def tika = new org.apache.tika.Tika()
			render("name="+f.absolutePath+ "<hr>")
			try {
				def parser = new AutoDetectParser()
				def metadata = new Metadata()
				def handler = new BodyContentHandler(20*1024*1024)
				parser.parse(inputStream, handler, metadata)
				def detectedMime = tika.detect(f)
				render(MetadataDef.getTypeByMime(detectedMime))
				def attributes = [:]
				metadata.names().each { name->
					attributes[name] = metadata.get(name)
					//render("name="+name + " and value = "+metadata.get(name) + "<BR>")
				}
				def attr = attributes as JSON
				render(attr.toString())
				render(handler.toString())
				//render(metadata)

			} catch (Exception e)
			{
				render(e)
			}

		}
}
