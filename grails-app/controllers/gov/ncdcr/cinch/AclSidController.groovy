package gov.ncdcr.cinch

import org.codehaus.groovy.grails.plugins.springsecurity.acl.AclSid
import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class AclSidController extends grails.plugins.springsecurity.ui.AclSidController {

	protected String lookupClassName() { AclSid.name }

	protected Class<?> lookupClass() { AclSid }
}
