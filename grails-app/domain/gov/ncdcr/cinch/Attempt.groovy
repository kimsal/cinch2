package gov.ncdcr.cinch

class Attempt {

	Date dateCreated
	Date lastUpdated
	String status
	File file

	static constraints = {
	}

	/**
	 * the most recent status set will always be
	 * applied to the parent file's status
	 * @return
	 */
	def beforeInsert()
	{
		file.status=status
	}
	def beforeUpdate()
	{
		file.status=status
	}
}
