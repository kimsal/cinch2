package gov.ncdcr.cinch

class Fileset {

	User user
	Date dateCreated
	Date lastUpdated
	String name
	Integer filecount = 0
	
	static hasMany = [files: File]
    static constraints = {
		name(nullable: true)
    }
}
