package gov.ncdcr.cinch

class Metadata {

	String raw
	String json
	String rawText
	Date dateCreated
	Date lastUpdated
	File file

	static belongsTo = [file: File]

    static constraints = {
		raw(maxSize: 50000, nullable:true)
		rawText(maxSize: 5000000, nullable:true)
		json(maxSize: 100000, nullable: true)
    }
}
