package gov.ncdcr.cinch

class File {

	String url
	String name
	String hash
	String path
	String downloadedName
	Integer size
	Date dateCreated
	Date lastUpdated
	Boolean processed = false
	String status
	String mime
	Metadata metadata
	
	static belongsTo = [fileset: Fileset]
	static hasMany = [attempts: Attempt]

    static constraints = {
		name(nullable: true)
		downloadedName(nullable: true)
		hash(nullable: true)
		path(nullable: true)
		status(nullable: true)
		size(nullable: true)
		mime(nullable: true)
		metadata(nullable: true)
    }
}
