<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta content="main" name="layout"/>
  <title></title>
</head>
<body>

<h2>File management</h2>
<hr/>
<h3>Upload a file to process</h3>
<g:form method="post" action="upload" enctype="multipart/form-data">

    Text file of URLs: <input type='file' name='upload'/>
    <br/>
    <g:submitButton name="submit" value="Upload"/>
</g:form>
<hr/>
<h2>Existing uploaded filesets</h2>

<table>
    <tr>
        <th></th>
        <th>File name</th>
        <th>Number of files</th>
        <th>DONE</th>
        <th>SIZE</th>
        <th>ERRORS</th>
        <th>NOT DONE</th>
        <th>Date uploaded</th>
    </tr>
<g:each in="${filesets}" var="fs">
    <%
        def countDone = 0
		def fileSize = 0
        def countErrors = 0
        def countNotDone = 0
		fs.files.each { ff->
			if(ff.status=='done') {
				countDone++
				fileSize+=ff.size
			}
			if(ff.status!='done' && ff.status!=null) {
				countErrors++
			}
			if(ff.status==null){
				countNotDone++
			}
		}
		def byteSize = gov.ncdcr.cinch.Helpers.humanReadableByteCount(fileSize, true)
		
        def backcss = "notdone"
        if(countNotDone==0)
        {
            backcss = "done"
            if(countErrors>0)
            {
                backcss = "doneerrors"
            }
        }

    %>
    <tr class="${backcss}">
        <td>

        </td>
        <td>${fs.name}</td>
        <td>${fs.filecount}</td>
        <td>${countDone}</td>
		<td>${byteSize}</td>
        <td><g:link action="viewErrors" id="${fs.id}">${countErrors}</g:link></td>
        <td><g:link action="viewNotDone" id="${fs.id}">${countNotDone}</g:link></td>
        <td>${fs.dateCreated}</td>
    </tr>
</g:each>    
</table>

</body>
</html>
