<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta content="main" name="layout"/>
  <title></title>
</head>
<body>
<h2>Files not yet completed for fileset ${fileset.name} -
uploaded
<g:formatDate date="${fileset.dateCreated}" format="MM/dd/yyyy hh:mm a"/>
</h2>
<g:form method="post" action="updatestatus">
<table>
    <tr>
        <th>File</th>
        <th>Status</th>
        <th>Date</th>
    </tr>
    <g:each in="${fileset.files.findAll{it.status==null}.sort{it.dateCreated}}" var="f">

        <tr>
            <td>
                <g:checkBox name="f.${f.id}"></g:checkBox>
                <a href="${f.url}" target="_new">${f.url}</a>
            </td>
            <td>${f.status}</td>
            <td>
                <g:formatDate date="${f.dateCreated}" format="MM/dd/yyyy hh:mm a"/>
            </td>
        </tr>

    </g:each>
</table>
    <g:submitButton name="submit" value="Retry selected files"/>
</g:form>

</body>
</html>