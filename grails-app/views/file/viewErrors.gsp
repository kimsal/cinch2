<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta content="main" name="layout"/>
  <title></title>
</head>
<body>
   <h2>Errors for fileset ${fileset.name} -
       uploaded
       <g:formatDate date="${fileset.dateCreated}" format="MM/dd/yyyy hh:mm a"/>
   </h2>
<g:form method="post" action="updatestatus">
<table>
    <tr>
        <th>File</th>
        <th>Error</th>
        <th>Date</th>
    </tr>
<g:each in="${fileset.files.findAll{it.status!=null && it.status!='done'}.sort{it.url}}" var="f">
    <tr>
                <td class="tableFileUrl" colspan="3">
    <g:checkBox name="f.${f.id}"></g:checkBox>
    <a href="${f.url}" target="_new">${f.url}</a>
    </td>
    </tr>
    <g:each in="${f.attempts.sort{it.dateCreated}}" var="a">
            <tr>
                <td class="tableFileUrl">&nbsp;</td>
            <td>${a.status}</td>
            <td>
                <g:formatDate date="${a.dateCreated}" format="MM/dd/yyyy hh:mm:ss a"/>
            </td>
        </tr>
    </g:each>
    <tr>
        <td colspan="3"><hr/></td>
    </tr>
</g:each>
</table>

<g:submitButton name="submit" value="Retry selected files"/>
</g:form>

</body>
</html>