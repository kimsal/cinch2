package gov.ncdcr.cinch

class MetadataDef {

	static LinkedHashMap info()
		{
		[
		'PDF':'application/pdf',
		'WORD':'application/msword',
		'WORD2007':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'PPT':'application/vnd.ms-powerpoint',
		'PPT2007':'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'EXCEL':'application/vnd.ms-excel',
		'EXCEL2007':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'JPEG':'image/jpeg',
		'PNG':'image/png',
		'GIF':'image/gif',
		'TEXT':'text/plain'
		]	
	}
	
	static getTypeByMime(mime)
	{
		def i = info()
		i.find {k,v->v==mime}?.key
	}
}
