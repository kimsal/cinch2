package gov.ncdcr.cinch

/**
 * from http://groovy.codehaus.org/Simple+file+download+from+URL
 */
class FileBinaryCategory {
	def static leftShift(java.io.File file, URL url) {
		println "START reading from " + url + " at " + new Date()
		/*
			def is = url.newInputStream(connectTimeout:3000, readTimeout:3000)
//			url.withInputStream {is->
				file.withOutputStream {os->
					def bs = new BufferedOutputStream( os )
					bs << is
				}
			//}
			*/

		url.withInputStream {is ->
			file.withOutputStream {os ->
				def bs = new BufferedOutputStream(os)
				bs << is
			}
		}

		println "DONE reading from " + url + " at " + new Date()
	}
}